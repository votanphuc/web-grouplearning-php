<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Thread</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/mystyle.css" />
	<script src="js/myjs.js"></script>


     <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>1 Col Portfolio - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <!-- Page Content -->
        <div class="container">

        <script language="javascript">
            function load_ajax()
            {
                // Tạo một biến lưu trữ đối tượng XML HTTP. Đối tượng này
                // tùy thuộc vào trình duyệt browser ta sử dụng nên phải kiểm
                // tra như bước bên dưới
                var xmlhttp;
                 
                // Nếu trình duyệt là  IE7+, Firefox, Chrome, Opera, Safari
                if (window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                // Nếu trình duyệt là IE6, IE5
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                 
                // Khởi tạo một hàm gửi ajax
                xmlhttp.onreadystatechange = function()
                {
                    // Nếu đối tượng XML HTTP trả về với hai thông số bên dưới thì mọi chuyện
                    // coi như thành công
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        // Sau khi thành công tiến hành thay đổi nội dung của thẻ div, nội dung
                        // ở đây chính là
                        document.getElementById("result").innerHTML = xmlhttp.responseText;
                    }
                };
                 
                // Khai báo với phương thức GET, và url chính là file result.php
                xmlhttp.open("GET", "test2echo.php", true);
                 
                // Cuối cùng là Gửi ajax, sau khi gọi hàm send thì function vừa tạo ở
                // trên (onreadystatechange) sẽ được chạy
                xmlhttp.send();
            }
        </script>
 
        <div id="result">
            Nội dung ajax sẽ được load ở đây
        </div>
        <input type="button" name="clickme" id="clickme" onclick="load_ajax()" value="Click Me"/>
   
<form action='' method='GET' accept-charset="utf-8">
							<div class="tableSchedule">
								<table border="2" style="text-align:center">
									<caption><b>LỊCH HỌC RẢNH CỦA BẠN</b></caption>
									<thead>
										<tr>
											<th></th>
											<th>Thứ 2</th>
											<th>Thứ 3</th>
											<th>Thứ 4</th>
											<th>Thứ 5</th>
											<th>Thứ 6</th>
											<th>Thứ 7</th>
											<th>Chủ nhật</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Cả ngày</td>
											<td><input name="checkboxarr[]" type="checkbox" value="0" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="1" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="2" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="3" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="4" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="5" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="6" id="all" onClick="checkAllCheckBox(this)"></td>

										</tr>
										<tr>
											<td >Ca 1</td>
											<td><input name="checkboxarr[]" type="checkbox" value="11" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="12" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="13" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="14" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="15" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="16" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="17" id="test"></td>
										</tr>
										<tr>
											<td>Ca 2</td>
											<td><input name="checkboxarr[]" type="checkbox" value="21" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="22" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="23" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="24" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="25" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="26" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="27" id="test"></td>
										</tr>
										<tr>
											<td>Ca 3</td>
											<td><input name="checkboxarr[]" type="checkbox" value="31" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="32" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="33" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="34" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="35" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="36" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="37" id="test"></td>
										</tr>
										<tr>
											<td>Ca 4</td>
											<td><input name="checkboxarr[]" type="checkbox" value="41" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="42" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="43" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="44" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="45" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="46" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="47" id="test"></td>
										</tr>
										<tr>
											<td>Ca 5</td>
											<td><input name="checkboxarr[]" type="checkbox" value="51" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="52" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="53" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="54" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="55" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="56" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="57" id="test"></td>
										</tr>
									</tbody>
								</table>
							</div><!--End of CSSTable Generator-->
							<div align="right">
								<br>
                                <input type="hidden" id="topicID" name="topicID" value="<?php echo $_GET['topicID']; ?>" />
								<input type="submit" class="btn btn-warning" name="schedulebt" id="schedulebt" value="Gửi/Sửa Lịch học">
							</div>
						</form><!--End of Form submit schedule-->
                        <?php
						 if(isset($_GET['checkboxarr']))
						 {
							 if(isset($_GET['checkboxarr']))
							 $value = $_GET['checkboxarr'];
							 for($i = 1 ; $i <= count($value); $i++)
							 {
								 if($value[$i])
								echo	$value[$i]."-".$i."<br>";
							 }
								
						 }
						?>

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-3">
                    <script type="text/javascript">
                    ldo_id = 'ldo-widget-container';
                    ldo_width = '200';
                    ldo_height = 'auto';
                    </script>
                    <script type="text/javascript" src="http://laodong.com.vn:/Widget/JS/Widget.js"></script>
                    <script type="text/javascript" src="http://laodong.com.vn:/Widget/GenerateJS.aspx?zone=63&count=5&ntype=popular&dtype=1&target=0"></script>
                </div>
                <div class="col-lg-9">
                    <h1 class="page-header">Page Heading
                        <small>Secondary Text</small>
                    </h1>
                </div>
            </div>
            <!-- /.row -->

            <!-- Project One -->
            <div class="row">
                <div class="col-md-7">
                    <a href="#">
                        <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>Project One</h3>
                    <h4>Subheading</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium veniam exercitationem expedita laborum at voluptate. Labore, voluptates totam at aut nemo deserunt rem magni pariatur quos perspiciatis atque eveniet unde.</p>
                    <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
            </div>
            <!-- /.row -->

            <hr>

            <!-- Project Two -->
            <div class="row">
                <div class="col-md-7">
                    <a href="#">
                        <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>Project Two</h3>
                    <h4>Subheading</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, odit velit cumque vero doloremque repellendus distinctio maiores rem expedita a nam vitae modi quidem similique ducimus! Velit, esse totam tempore.</p>
                    <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
            </div>
            <!-- /.row -->

            <hr>

            <!-- Project Three -->
            <div class="row">
                <div class="col-md-7">
                    <a href="#">
                        <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>Project Three</h3>
                    <h4>Subheading</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, temporibus, dolores, at, praesentium ut unde repudiandae voluptatum sit ab debitis suscipit fugiat natus velit excepturi amet commodi deleniti alias possimus!</p>
                    <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
            </div>
            <!-- /.row -->

            <hr>

            <!-- Project Four -->
            <div class="row">

                <div class="col-md-7">
                    <a href="#">
                        <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>Project Four</h3>
                    <h4>Subheading</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, quidem, consectetur, officia rem officiis illum aliquam perspiciatis aspernatur quod modi hic nemo qui soluta aut eius fugit quam in suscipit?</p>
                    <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
            </div>
            <!-- /.row -->

            <hr>

            <!-- Project Five -->
            <div class="row">
                <div class="col-md-7">
                    <a href="#">
                        <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>Project Five</h3>
                    <h4>Subheading</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, quo, minima, inventore voluptatum saepe quos nostrum provident ex quisquam hic odio repellendus atque porro distinctio quae id laboriosam facilis dolorum.</p>
                    <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
            </div>
            <!-- /.row -->

            <hr>

            <!-- Pagination -->
            <div class="row text-center">
                <div class="col-lg-12">
                    <ul class="pagination">
                        <li>
                            <a href="#">&laquo;</a>
                        </li>
                        <li class="active">
                            <a href="#">1</a>
                        </li>
                        <li>
                            <a href="#">2</a>
                        </li>
                        <li>
                            <a href="#">3</a>
                        </li>
                        <li>
                            <a href="#">4</a>
                        </li>
                        <li>
                            <a href="#">5</a>
                        </li>
                        <li>
                            <a href="#">&raquo;</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.row -->

            <hr>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>
            
            
<ul class="demo-2 effect">
    <li>
       <h2 class="zero">This is a cool title!</h2>
       <p class="zero">Lorem ipsum dolor sit amet.</p>
    </li>
    <li><img class="top" src="img/logo.png" alt=""/></li>
</ul>
        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </div>
</body>

</html>
