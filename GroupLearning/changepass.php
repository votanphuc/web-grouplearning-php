
<!DOCTYPE html>
<head>
<meta charset="utf-8">
<title>Change your Password</title>
<link rel="stylesheet" type="text/css" href="login.css" />
</head>
<body>
<?php
	ini_set("display_errors",0);
	include("header.php");
    include("models/xuly.php");
	if(!$_SESSION['user'])
			echo"<script>window.location.href = 'index.php'; alert('Bạn cần phải đăng nhập để có thể đổi mật khẩu! '); </script>";
	$t = new xuly;
	if(isset($_POST['submitchange'])){
		if($_POST["newpassword"] != $_POST['confirmpassword'])
		{
			?>
            <script>
            alert('Lỗi! Vui lòng kiểm tra Password của bạn!');
            </script>
            <?php	
		}
		else
		{
			$kq = $t->changePass($_POST["oldpassword"], $_POST["newpassword"], $_SESSION['user']);

			if($kq>0)
			
			{
				?>
				<script>
				alert('Đổi Password thành công');
				</script>
				<?php
				
			}
			else 
			{
				?>
				<script>
                alert('Lỗi! Password cũ của bạn không đúng');
                </script>
                <?php		
			}
			
		}
	}

?>
<div class="container">
	<section id="content">
		<form action="" name="changepassform" id="changepassform" method="post">
			<h1>Change Password</h1>
			<div>
				<input type="password" placeholder="Old Password" required="" id="oldpassword" name="oldpassword" />
			</div>
			<div>
				<input type="password" placeholder="New Password" required="" id="newpassword" name="newpassword" />
			</div>
            <div>
				<input type="password" placeholder="Confirm Password" required="" id="confirmpassword" name="confirmpassword" />
			</div>
			<div class="btcenter">
				<input type="submit" value="Change" id="submitchange" name="submitchange"/>	
			</div>
		</form><!-- form -->


		<div class="button" >
			<a href="index.php">Go to Homepage</a>
		</div><!-- button -->
	
	</section><!-- content -->
</div><!-- container -->
</body>
</html>	