-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2015 at 01:24 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `learninggroup`
--

-- --------------------------------------------------------

--
-- Table structure for table `liststudent`
--

CREATE TABLE IF NOT EXISTS `liststudent` (
`listID` tinyint(4) NOT NULL,
  `topicID` tinyint(4) NOT NULL,
  `stdID` varchar(8) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `liststudent`
--

INSERT INTO `liststudent` (`listID`, `topicID`, `stdID`) VALUES
(34, 90, '51203194'),
(35, 89, '51203104'),
(36, 90, '51203104'),
(37, 89, '51203194'),
(38, 89, 'admin'),
(42, 89, '51203100');

-- --------------------------------------------------------

--
-- Table structure for table `mess`
--

CREATE TABLE IF NOT EXISTS `mess` (
`messID` tinyint(4) NOT NULL,
  `stdID` varchar(8) NOT NULL,
  `TopicID` tinyint(4) NOT NULL,
  `mess` text NOT NULL,
  `dayMess` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mess`
--

INSERT INTO `mess` (`messID`, `stdID`, `TopicID`, `mess`, `dayMess`) VALUES
(3, '51203104', 90, 'Các bạn chuẩn bị giáo trình đầy đủ nhé!', '2015/05/26'),
(4, '51203194', 89, 'Môn này học cần những gì các bạn :)', '2015/05/26'),
(5, '51203194', 90, 'Không có giáo trình thì mượn ai thớt :3', '2015/05/26'),
(6, 'admin', 91, 'Anh em mau mau submit bài nhé', '2015/05/26');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `t0` tinyint(4) DEFAULT '0',
  `t1` tinyint(4) DEFAULT '0',
  `t2` tinyint(4) DEFAULT '0',
  `t3` tinyint(4) DEFAULT '0',
  `t4` tinyint(4) DEFAULT '0',
  `t5` tinyint(4) DEFAULT '0',
  `t6` tinyint(4) DEFAULT '0',
  `t7` tinyint(4) DEFAULT '0',
  `t8` tinyint(4) DEFAULT '0',
  `t9` tinyint(4) DEFAULT '0',
  `t10` tinyint(4) DEFAULT '0',
  `t11` tinyint(4) DEFAULT '0',
  `t12` tinyint(4) DEFAULT '0',
  `t13` tinyint(4) DEFAULT '0',
  `t14` tinyint(4) DEFAULT '0',
  `t15` tinyint(4) DEFAULT '0',
  `t16` tinyint(4) DEFAULT '0',
  `t17` tinyint(4) DEFAULT '0',
  `t18` tinyint(4) DEFAULT '0',
  `t19` tinyint(4) DEFAULT '0',
  `t20` tinyint(4) DEFAULT '0',
  `t21` tinyint(4) DEFAULT '0',
  `t22` tinyint(4) DEFAULT '0',
  `t23` tinyint(4) DEFAULT '0',
  `t24` tinyint(4) DEFAULT '0',
  `t25` tinyint(4) DEFAULT '0',
  `t26` tinyint(4) DEFAULT '0',
  `t27` tinyint(4) DEFAULT '0',
  `t28` tinyint(4) DEFAULT '0',
  `t29` tinyint(4) DEFAULT '0',
  `t30` tinyint(4) DEFAULT '0',
  `t31` tinyint(4) DEFAULT '0',
  `t32` tinyint(4) DEFAULT '0',
  `t33` tinyint(4) DEFAULT '0',
  `t34` tinyint(4) DEFAULT '0',
`scheID` tinyint(4) NOT NULL,
  `topicID` tinyint(4) DEFAULT NULL,
  `stdID` varchar(8) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`t0`, `t1`, `t2`, `t3`, `t4`, `t5`, `t6`, `t7`, `t8`, `t9`, `t10`, `t11`, `t12`, `t13`, `t14`, `t15`, `t16`, `t17`, `t18`, `t19`, `t20`, `t21`, `t22`, `t23`, `t24`, `t25`, `t26`, `t27`, `t28`, `t29`, `t30`, `t31`, `t32`, `t33`, `t34`, `scheID`, `topicID`, `stdID`) VALUES
(2, 0, 1, 1, 0, 0, 0, 0, 1, 2, 2, 1, 1, 0, 1, 1, 2, 1, 1, 2, 2, 0, 1, 1, 2, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 64, 89, '00000000'),
(1, 1, 2, 0, 0, 1, 1, 1, 2, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 2, 1, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 65, 90, '00000000'),
(0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 66, 90, '51203194'),
(1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 67, 89, '51203104'),
(1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 90, '51203104'),
(1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 89, '51203194'),
(0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 70, 89, 'admin'),
(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 71, 91, '00000000'),
(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 72, 91, 'admin'),
(0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 73, 89, '51203100');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `stdID` varchar(8) NOT NULL,
  `stdPass` varchar(20) NOT NULL,
  `stdSche` varchar(35) DEFAULT NULL,
  `topicID` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`stdID`, `stdPass`, `stdSche`, `topicID`) VALUES
('51203001', '123', NULL, NULL),
('51203002', '111111', NULL, NULL),
('51203003', 'abcd1234', NULL, NULL),
('51203004', 'abcd1234', NULL, NULL),
('51203005', 'abcd1234', NULL, NULL),
('51203006', 'abcd1234', NULL, NULL),
('51203007', 'abcd1234', NULL, NULL),
('51203008', 'abcd1234', NULL, NULL),
('51203009', 'abcd1234', NULL, NULL),
('51203010', 'abcd1234', NULL, NULL),
('51203011', 'abcd1234', NULL, NULL),
('51203012', 'abcd1234', NULL, NULL),
('51203013', 'abcd1234', NULL, NULL),
('51203014', 'abcd1234', NULL, NULL),
('51203015', 'abcd1234', NULL, NULL),
('51203016', 'abcd1234', NULL, NULL),
('51203017', 'abcd1234', NULL, NULL),
('51203018', 'abcd1234', NULL, NULL),
('51203019', 'abcd1234', NULL, NULL),
('51203020', 'abcd1234', NULL, NULL),
('51203021', 'abcd1234', NULL, NULL),
('51203022', 'abcd1234', NULL, NULL),
('51203023', 'abcd1234', NULL, NULL),
('51203024', 'abcd1234', NULL, NULL),
('51203025', 'abcd1234', NULL, NULL),
('51203026', 'abcd1234', NULL, NULL),
('51203027', 'abcd1234', NULL, NULL),
('51203028', 'abcd1234', NULL, NULL),
('51203029', 'abcd1234', NULL, NULL),
('51203030', 'abcd1234', NULL, NULL),
('51203031', 'abcd1234', NULL, NULL),
('51203032', 'abcd1234', NULL, NULL),
('51203033', 'abcd1234', NULL, NULL),
('51203034', 'abcd1234', NULL, NULL),
('51203035', 'abcd1234', NULL, NULL),
('51203036', 'abcd1234', NULL, NULL),
('51203037', 'abcd1234', NULL, NULL),
('51203038', 'abcd1234', NULL, NULL),
('51203039', 'abcd1234', NULL, NULL),
('51203040', 'abcd1234', NULL, NULL),
('51203041', 'abcd1234', NULL, NULL),
('51203042', 'abcd1234', NULL, NULL),
('51203043', 'abcd1234', NULL, NULL),
('51203044', 'abcd1234', NULL, NULL),
('51203045', 'abcd1234', NULL, NULL),
('51203046', 'abcd1234', NULL, NULL),
('51203047', 'abcd1234', NULL, NULL),
('51203048', 'abcd1234', NULL, NULL),
('51203049', 'abcd1234', NULL, NULL),
('51203050', 'abcd1234', NULL, NULL),
('51203051', 'abcd1234', NULL, NULL),
('51203052', 'abcd1234', NULL, NULL),
('51203053', 'abcd1234', NULL, NULL),
('51203054', 'abcd1234', NULL, NULL),
('51203055', 'abcd1234', NULL, NULL),
('51203056', 'abcd1234', NULL, NULL),
('51203057', 'abcd1234', NULL, NULL),
('51203058', 'abcd1234', NULL, NULL),
('51203059', 'abcd1234', NULL, NULL),
('51203060', 'abcd1234', NULL, NULL),
('51203061', 'abcd1234', NULL, NULL),
('51203062', 'abcd1234', NULL, NULL),
('51203063', 'abcd1234', NULL, NULL),
('51203064', 'abcd1234', NULL, NULL),
('51203065', 'abcd1234', NULL, NULL),
('51203066', 'abcd1234', NULL, NULL),
('51203067', 'abcd1234', NULL, NULL),
('51203068', 'abcd1234', NULL, NULL),
('51203069', 'abcd1234', NULL, NULL),
('51203070', 'abcd1234', NULL, NULL),
('51203071', 'abcd1234', NULL, NULL),
('51203072', 'abcd1234', NULL, NULL),
('51203073', 'abcd1234', NULL, NULL),
('51203074', 'abcd1234', NULL, NULL),
('51203075', 'abcd1234', NULL, NULL),
('51203076', 'abcd1234', NULL, NULL),
('51203077', 'abcd1234', NULL, NULL),
('51203078', 'abcd1234', NULL, NULL),
('51203079', 'abcd1234', NULL, NULL),
('51203080', 'abcd1234', NULL, NULL),
('51203081', 'abcd1234', NULL, NULL),
('51203082', 'abcd1234', NULL, NULL),
('51203083', 'abcd1234', NULL, NULL),
('51203084', 'abcd1234', NULL, NULL),
('51203085', 'abcd1234', NULL, NULL),
('51203086', 'abcd1234', NULL, NULL),
('51203087', 'abcd1234', NULL, NULL),
('51203088', 'abcd1234', NULL, NULL),
('51203089', 'abcd1234', NULL, NULL),
('51203090', 'abcd1234', NULL, NULL),
('51203091', 'abcd1234', NULL, NULL),
('51203092', 'abcd1234', NULL, NULL),
('51203093', 'abcd1234', NULL, NULL),
('51203094', 'abcd1234', NULL, NULL),
('51203095', 'abcd1234', NULL, NULL),
('51203096', 'abcd1234', NULL, NULL),
('51203097', 'abcd1234', NULL, NULL),
('51203098', 'abcd1234', NULL, NULL),
('51203099', 'abcd1234', NULL, NULL),
('51203100', 'abcd1234', NULL, NULL),
('51203101', 'abcd1234', NULL, NULL),
('51203102', 'abcd1234', NULL, NULL),
('51203103', 'abcd1234', NULL, NULL),
('51203104', 'abcd1234', NULL, NULL),
('51203105', 'abcd1234', NULL, NULL),
('51203106', 'abcd1234', NULL, NULL),
('51203107', 'abcd1234', NULL, NULL),
('51203108', 'abcd1234', NULL, NULL),
('51203109', 'abcd1234', NULL, NULL),
('51203110', 'abcd1234', NULL, NULL),
('51203111', 'abcd1234', NULL, NULL),
('51203112', 'abcd1234', NULL, NULL),
('51203113', 'abcd1234', NULL, NULL),
('51203114', 'abcd1234', NULL, NULL),
('51203115', 'abcd1234', NULL, NULL),
('51203116', 'abcd1234', NULL, NULL),
('51203117', 'abcd1234', NULL, NULL),
('51203118', 'abcd1234', NULL, NULL),
('51203119', 'abcd1234', NULL, NULL),
('51203120', 'abcd1234', NULL, NULL),
('51203121', 'abcd1234', NULL, NULL),
('51203122', 'abcd1234', NULL, NULL),
('51203123', 'abcd1234', NULL, NULL),
('51203124', 'abcd1234', NULL, NULL),
('51203125', 'abcd1234', NULL, NULL),
('51203126', 'abcd1234', NULL, NULL),
('51203127', 'abcd1234', NULL, NULL),
('51203128', 'abcd1234', NULL, NULL),
('51203129', 'abcd1234', NULL, NULL),
('51203130', 'abcd1234', NULL, NULL),
('51203131', 'abcd1234', NULL, NULL),
('51203132', 'abcd1234', NULL, NULL),
('51203133', 'abcd1234', NULL, NULL),
('51203134', 'abcd1234', NULL, NULL),
('51203135', 'abcd1234', NULL, NULL),
('51203136', 'abcd1234', NULL, NULL),
('51203137', 'abcd1234', NULL, NULL),
('51203138', 'abcd1234', NULL, NULL),
('51203139', 'abcd1234', NULL, NULL),
('51203140', 'abcd1234', NULL, NULL),
('51203141', 'abcd1234', NULL, NULL),
('51203142', 'abcd1234', NULL, NULL),
('51203143', 'abcd1234', NULL, NULL),
('51203144', 'abcd1234', NULL, NULL),
('51203145', 'abcd1234', NULL, NULL),
('51203146', 'abcd1234', NULL, NULL),
('51203147', 'abcd1234', NULL, NULL),
('51203148', 'abcd1234', NULL, NULL),
('51203149', 'abcd1234', NULL, NULL),
('51203150', 'abcd1234', NULL, NULL),
('51203151', 'abcd1234', NULL, NULL),
('51203152', 'abcd1234', NULL, NULL),
('51203153', 'abcd1234', NULL, NULL),
('51203154', 'abcd1234', NULL, NULL),
('51203155', 'abcd1234', NULL, NULL),
('51203156', 'abcd1234', NULL, NULL),
('51203157', 'abcd1234', NULL, NULL),
('51203158', 'abcd1234', NULL, NULL),
('51203159', 'abcd1234', NULL, NULL),
('51203160', 'abcd1234', NULL, NULL),
('51203161', 'abcd1234', NULL, NULL),
('51203162', 'abcd1234', NULL, NULL),
('51203163', 'abcd1234', NULL, NULL),
('51203164', 'abcd1234', NULL, NULL),
('51203165', 'abcd1234', NULL, NULL),
('51203166', 'abcd1234', NULL, NULL),
('51203167', 'abcd1234', NULL, NULL),
('51203168', 'abcd1234', NULL, NULL),
('51203169', 'abcd1234', NULL, NULL),
('51203170', 'abcd1234', NULL, NULL),
('51203171', 'abcd1234', NULL, NULL),
('51203172', 'abcd1234', NULL, NULL),
('51203173', 'abcd1234', NULL, NULL),
('51203174', 'abcd1234', NULL, NULL),
('51203175', 'abcd1234', NULL, NULL),
('51203176', 'abcd1234', NULL, NULL),
('51203177', 'abcd1234', NULL, NULL),
('51203178', 'abcd1234', NULL, NULL),
('51203179', 'abcd1234', NULL, NULL),
('51203180', 'abcd1234', NULL, NULL),
('51203181', 'abcd1234', NULL, NULL),
('51203182', 'abcd1234', NULL, NULL),
('51203183', 'abcd1234', NULL, NULL),
('51203184', 'abcd1234', NULL, NULL),
('51203185', 'abcd1234', NULL, NULL),
('51203186', 'abcd1234', NULL, NULL),
('51203187', 'abcd1234', NULL, NULL),
('51203188', 'abcd1234', NULL, NULL),
('51203189', 'abcd1234', NULL, NULL),
('51203190', 'abcd1234', NULL, NULL),
('51203191', 'abcd1234', NULL, NULL),
('51203192', 'abcd1234', NULL, NULL),
('51203193', 'abcd1234', NULL, NULL),
('51203194', 'abcd1234', NULL, NULL),
('51203195', 'abcd1234', NULL, NULL),
('51203196', 'abcd1234', NULL, NULL),
('51203197', 'abcd1234', NULL, NULL),
('51203198', 'abcd1234', NULL, NULL),
('51203199', 'abcd1234', NULL, NULL),
('51203200', 'abcd1234', NULL, NULL),
('51203201', 'abcd1234', NULL, NULL),
('51203202', 'abcd1234', NULL, NULL),
('51203203', 'abcd1234', NULL, NULL),
('51203204', 'abcd1234', NULL, NULL),
('51203205', 'abcd1234', NULL, NULL),
('51203206', 'abcd1234', NULL, NULL),
('51203207', 'abcd1234', NULL, NULL),
('51203208', 'abcd1234', NULL, NULL),
('51203209', 'abcd1234', NULL, NULL),
('51203210', 'abcd1234', NULL, NULL),
('51203211', 'abcd1234', NULL, NULL),
('51203212', 'abcd1234', NULL, NULL),
('51203213', 'abcd1234', NULL, NULL),
('51203214', 'abcd1234', NULL, NULL),
('51203215', 'abcd1234', NULL, NULL),
('51203216', 'abcd1234', NULL, NULL),
('51203217', 'abcd1234', NULL, NULL),
('51203218', 'abcd1234', NULL, NULL),
('51203219', 'abcd1234', NULL, NULL),
('51203220', 'abcd1234', NULL, NULL),
('51203221', 'abcd1234', NULL, NULL),
('51203222', 'abcd1234', NULL, NULL),
('51203223', 'abcd1234', NULL, NULL),
('51203224', 'abcd1234', NULL, NULL),
('51203225', 'abcd1234', NULL, NULL),
('51203226', 'abcd1234', NULL, NULL),
('51203227', 'abcd1234', NULL, NULL),
('51203228', 'abcd1234', NULL, NULL),
('51203229', 'abcd1234', NULL, NULL),
('51203230', 'abcd1234', NULL, NULL),
('51203231', 'abcd1234', NULL, NULL),
('51203232', 'abcd1234', NULL, NULL),
('51203233', 'abcd1234', NULL, NULL),
('51203234', 'abcd1234', NULL, NULL),
('51203235', 'abcd1234', NULL, NULL),
('51203236', 'abcd1234', NULL, NULL),
('51203237', 'abcd1234', NULL, NULL),
('51203238', 'abcd1234', NULL, NULL),
('51203239', 'abcd1234', NULL, NULL),
('51203240', 'abcd1234', NULL, NULL),
('51203241', 'abcd1234', NULL, NULL),
('51203242', 'abcd1234', NULL, NULL),
('51203243', 'abcd1234', NULL, NULL),
('51203244', 'abcd1234', NULL, NULL),
('51203245', 'abcd1234', NULL, NULL),
('51203246', 'abcd1234', NULL, NULL),
('51203247', 'abcd1234', NULL, NULL),
('51203248', 'abcd1234', NULL, NULL),
('51203249', 'abcd1234', NULL, NULL),
('51203250', 'abcd1234', NULL, NULL),
('admin', '12345', NULL, NULL),
('user1', 'pass1', NULL, NULL),
('user2', 'pass2', NULL, NULL),
('user3', 'pass3', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE IF NOT EXISTS `topic` (
`topicID` tinyint(4) NOT NULL,
  `stdID` varchar(8) NOT NULL,
  `topName` text NOT NULL,
  `description` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`topicID`, `stdID`, `topName`, `description`) VALUES
(89, '51203104', 'Nhóm Học WEB - Lớp của thầy Tín mập', 'Các bạn vào đăng ký lịch học rảnh của mình để tham gia học tìm hiểu để chuẩn bị cho bài thi được tốt nhé!\r\nSắp thi giữa kỳ rồi các đồng chí ơi.\r\nCố gắng lên nhé'),
(90, '51203194', 'Ôn tập Môn Android', 'Các đồng chí hỡi ơi!\r\nBơi vào đây mà coi.\r\nAndroid hông được là toi\r\nTập trung chuyên môn nhé :)'),
(91, 'admin', 'Chuyên đề JAV-a', 'Ngày khai tử đã đến!\r\nGiờ lành đã điểm;\r\ncác đồng chí tập hợp. ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `liststudent`
--
ALTER TABLE `liststudent`
 ADD PRIMARY KEY (`listID`);

--
-- Indexes for table `mess`
--
ALTER TABLE `mess`
 ADD PRIMARY KEY (`messID`), ADD KEY `fkStdIDMess` (`stdID`), ADD KEY `fkTopicID` (`TopicID`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
 ADD PRIMARY KEY (`scheID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`stdID`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
 ADD PRIMARY KEY (`topicID`), ADD KEY `fkStdID` (`stdID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `liststudent`
--
ALTER TABLE `liststudent`
MODIFY `listID` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `mess`
--
ALTER TABLE `mess`
MODIFY `messID` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
MODIFY `scheID` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
MODIFY `topicID` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `mess`
--
ALTER TABLE `mess`
ADD CONSTRAINT `fkStdIDMess` FOREIGN KEY (`stdID`) REFERENCES `student` (`stdID`),
ADD CONSTRAINT `fkTopicID` FOREIGN KEY (`TopicID`) REFERENCES `topic` (`topicID`);

--
-- Constraints for table `topic`
--
ALTER TABLE `topic`
ADD CONSTRAINT `fkStdID` FOREIGN KEY (`stdID`) REFERENCES `student` (`stdID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
