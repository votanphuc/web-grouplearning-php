<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TDT Learning Group</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="css/mystyle.css" />

</head>
<body style="background-color: #aaf0d1">
    <div class="container">
        <br><hr color="#0000FF">
        <div class="row">
            <div class="col-md-2 demo-3">
                <figure>
                    <img class="img-rounded" src="img/it.png" onClick="window.location.href='http://it.tdt.edu.vn'" width="175px" height="150px">
                    <figcaption>
                        <h2>Trang chủ - Khoa CNTT</h2>
                    </figcaption>         
                </figure>
            </div>	
            <div class="col-md-2 demo-3">
                <figure>
                    <img class="img-rounded" src="img/tdt.jpg" onClick="window.location.href='http://tdt.edu.vn'" width="175px" height="150px">
                    <figcaption>
                        <h2>Trang chủ - ĐH Tôn Đức Thắng</h2>
                    </figcaption>         
                </figure>
            </div>           
            <div class="col-md-2 demo-3">
                <figure>
                    <img class="img-rounded" src="img/fb.png" onClick="window.location.href='https://www.facebook.com/groups/131752173571578/'" width="175px" height="150px">
                    <figcaption>
                        <h2>Group Facebook - Khoa CNTT</h2>
                    </figcaption>         
                </figure>
            </div>	
            <div class="col-md-2 demo-3">
                <figure>
                    <img class="img-rounded" src="img/sv.png" onClick="window.location.href='https://student.tdt.edu.vn'" width="175px" height="150px">
                    <figcaption>
                        <h2>Hệ thống Thông tin sinh viên</h2>
                    </figcaption>         
                </figure>
            </div>	
            <div class="col-md-2 demo-3">
                <figure>
                    <img class="img-rounded" src="img/stdt.jpg" onClick="window.location.href='http://stdt.vn'" width="175px" height="150px">
                    <figcaption>
                        <h2>STDT Communications</h2>
                    </figcaption>         
                </figure>
            </div>	
            <div class="col-md-2 demo-3">
                <figure>
                    <img class="img-rounded" src="img/ttc.png" onClick="window.location.href='https://ttc.tdt.edu.vn'" width="175px" height="150px">
                    <figcaption>
                        <h2>Trung tâm TOEIC	</h2>
                    </figcaption>         
                </figure>
            </div>	
        </div>
        <!--div id="footer" style="background-image:url(img/footer.png); width:1175px; height:138px; text-align:right; margin-bottom:0px; " -->
        <div id="footer" style="background-color:#066; text-align:center" >
            Copyright &copy; VoTanPhuc  <br> Student at TDTU<br>                
            Contact: 0988610379 - Email: votanphuc@gmail.com
        </div>
    </div>      
</body>
</html>