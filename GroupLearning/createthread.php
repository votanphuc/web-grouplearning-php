<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Create Thread</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/mystyle.css" />

</head>
<body style="background-color: #8FD6FF">
	<div class="container">
		<?php
		ini_set("display_errors",0);
		include("header.php");
		include("models/xuly.php");
		$t = new xuly;
		if(!$_SESSION['user'])
			echo"<script>window.location.href = 'login.php'; alert('Bạn phải đăng nhập để có thể tạo Nhóm mới!'); </script>";
		if(isset($_POST['submit']))
		{
			
			

			if( $_POST['topic'] =="" || $_POST['description']=="")
			{
				?>
				<script>
					alert('Lỗi Khởi tạo Nhóm học tập'); //sai kieu du lieu, null
					//window.location = "index.php";
				</script>
				<?php	
			}
			else
			{	
				$kq = $t->addTopic($_SESSION['user'], $_POST['topic'],$_POST['description']);
				$idTop = $t->getIDTopic($_SESSION['user'], $_POST['topic'],$_POST['description']);
				$row=mysql_fetch_array($idTop);
				$kq2 = $t->addScheSum($row[0]);
				if($kq)
				{
					?>
					<script>
					alert('Tạo Nhóm học tập thành công');
					window.location = "index.php"
					</script>
					<?php
				}
				else
				{
					?>
					<script>
                        alert('Lỗi tạo Nhóm học tập.'); //sai kieu du lieu, null
                        //window.location = "index.php";
                    </script>
                    <?php
				}
			}
		}
		?>	
		<div class="row span">

			<form class="form-horizontal" id="createtopic" method='post' action=''>
				<fieldset>
					<div class=" panel panel-success">
						<div class="panel-heading">
							<legend>Tạo nhóm học tập mới:</legend>
						</div>
						<div class="panel-body">								
							<h4>Tên Nhóm học tập:</h4>
							<input type="text" class="form-control" id="topic" name="topic" size="100%">
							<h4>Mô tả:</h4>
							<textarea name="description" id="description" cols="150" rows="10" class="form-control custom-control" style="resize:none"></textarea>								
							<br>
							<div class="control-group">
								<label class="control-label">Đảm bảo rằng tên, mô tả được điền đầy đủ. Tên Nhóm cần mang tính gợi nhớ để sinh viên có thể dễ dàng Tham gia vào Nhóm!</label>
								<div class="controls">
									<input type="submit" class="btn btn-success" name="submit" value="Tạo Mới" />
								</div>
							</div>
						</div><!--End Panel Body-->
					</div>
				</fieldset> 
			</form>
		</div><!--End row span-->



		<?php 
		include("footer.php");
		?>
	</div>


</body>
</html>