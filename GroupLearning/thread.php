<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Nhóm học tập</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/mystyle.css" />
	<script src="js/myjs.js"></script>

</head>
<body style="background-color: #8FD6FF">
	<div class="container">
		<?php
		include("header.php");  
		ini_set("display_errors",0);
		include("models/xuly.php");
		 
		if(!$_GET['topicID']) // nếu id rỗng thì không cho vào về lại index
			echo"<script>window.location.href = 'index.php';</script>";
		$topicID = $_GET['topicID'];
		$flag = false; // gắn cờ kiểm tra xem session này đã submit lịch học trước đó hay chưa
		$t = new xuly;
		
		if(isset($_POST['sentchat']))
		{
			if(!$_SESSION['user'])
				echo"<script>alert('Bạn phải đăng nhập để có thể sử dụng Chatbox!'); </script>";
			else if($_POST['mess'] == "")
				echo"<script>alert('Message rỗng!'); </script>";
			else
			{				
				$kq = $t->addMessage($_SESSION['user'],$_GET['topicID'], $_POST['mess']);
			}	
		}
		$t->updateTopic($_GET['topicID']);
		$kq2 = $t->showScheduleAll($_GET['topicID']);
		$row2 = mysql_fetch_array($kq2); //lay thoi khoa bieu chung
		
		
		$kq = $t->showTopic2($_GET['topicID']);
		$row= mysql_fetch_array($kq); //lay thong tin cua topic trong do co ten topic
		if($row[2] == '') echo"<script>window.location.href = 'index.php';</script>"; // nếu topic chưa tồn tại, chưa có trong db, thì cho về index
		?><br>

		<div class="panel panel-primary">
			<div class="panel-heading"><!--Title Thread-->
				<legend><?php echo $row[2];?></legend>
			</div><!--End of Title Thread-->
			<div class="panel-body">
            <h4><span class="label label-warning">Description: </span></h4>
            <?php echo$row[3]?>
				<div class="row "> <!--Lich hoc chung-->
                	<div class="col-lg-10 CSSTableGenerator">
                        <table border="1" >
                            <caption ><h1 class="headText">LỊCH HỌC CHUNG</h1></caption>
                            <tbody>
                                <tr>
                                    <td>#</td>
                                    <td>Thứ 2</td>
                                    <td>Thứ 3</td>
                                    <td>Thứ 4</td>
                                    <td>Thứ 5</td>
                                    <td>Thứ 6</td>
                                    <td>Thứ 7</td>
                                    <td>Chủ nhật</td>
                                </tr>
                                <?php
								$listStd = $t->showList($_GET['topicID']);
								$numStd = mysql_num_rows($listStd);
                                for($j = 0; $j < 5; $j++)
                                {
                                    echo "<tr> <td  style='text-align:center; font-size:17px; color:#008080; font-weight:bold' > Ca " ;
                                    echo $j+1;
                                    echo" </td>";
                                    for($i = 0; $i < 7;$i++)
                                    {
                                        $v = $row2[$i + $j*7];
										if($v == $numStd && $numStd != 0)
										 	echo "<td  id='num' style=\"text-align:center; background-color:#FF0;\"';>".$v."/".$numStd."</td>";
										else
                                        	echo "<td  id='num' style=\"text-align:center;\"';>".$v."/".$numStd."</td>";
                                    }
                                    echo"</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!--CSSTabbleGenerator-->
                    <div class="col-lg-2">
					<h4 class="headText">Danh sách<br> Thành viên</h4>
					<textarea name='liststd' id='liststd' cols='5' rows='11' class='form-control custom-control' style='resize:none' readonly>
                    <?php
					$list = $t->showList($_GET['topicID']);
					while($row = mysql_fetch_array($list))
					{
						echo $row[2] . "\n";
						if($_SESSION['user'] == $row[2])
							$flag = true;
					}
					?>
                    </textarea>
                    </div>

				</div> <!--End of Lich hoc chung -->
				<div class="row">
					<div class="col-lg-6"><!--Public Chat-->
						<hr color="#0000FF">
						Bạn có thể để lại Lời nhắn để biết thêm thông tin về Nhóm học tập, ngày giờ, tài liệu, số thành viên,... trước khi bạn quyết định Gửi lịch học của mình            
						<form action="" method="post" id="chatbox"><!--chatbox form-->
							<h4>Chatbox:</h4>
                            
							<textarea name='chatbox' id='chatbox' cols='150' rows='20' class='form-control custom-control' style='resize:none' readonly>
                            <?php
                            	$kq = $t->showMessage($_GET['topicID']);
								echo"\n";
								while($row=mysql_fetch_array($kq))
								echo $row[1].": ". $row[3] ."\n";
							?>
                            
                            </textarea> 

							<input type="text" style="width:90%" id="mess" name="mess"/>
							<input type="submit" id="sentchat" name="sentchat">
						</form>	<!--End of chatbox Form-->		
					</div><!--End of Public chat-->
                    
                    
                    
                    
					<div class="col-lg-6"><!--Schedule-->
						<hr color="#0000FF">
                        Thêm hoặc Sửa lịch học của bạn.<br>
						Nếu Nhóm học tập phù hợp với bạn, bạn có thể để lại lịch học của mình bằng cách chọn thời gian rỗi theo các ca trong ngày, và các ngày trong tuần, 1 tuần có 7 buổi và 1 buổi có 5 ca. Sau đó nhấn nút "Gửi/Sửa Lịch học"<br>
						Bạn có thể Sửa đổi lịch học của bạn bằng cách tích chọn lại thời gian rỗi và nhấn nút "Gửi/Sửa Lịch học"<br>
						Ngoài ra bạn có thể để lại thông tin hoặc câu hỏi của bạn tại Chatbox để được các bạn trong nhóm học tập giải đáp.
						<hr color="#0000FF">

						<form action='' method='POST' accept-charset="utf-8">
							<div class="tableSchedule">
								<table border="2" style="text-align:center">
									<caption><b>LỊCH HỌC RẢNH CỦA BẠN</b></caption>
									<thead>
										<tr>
											<th></th>
											<th>Thứ 2</th>
											<th>Thứ 3</th>
											<th>Thứ 4</th>
											<th>Thứ 5</th>
											<th>Thứ 6</th>
											<th>Thứ 7</th>
											<th>Chủ nhật</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Cả ngày</td>
											<td><input type="checkbox" value="100" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input type="checkbox" value="101" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input type="checkbox" value="102" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input type="checkbox" value="103" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input type="checkbox" value="104" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input type="checkbox" value="105" id="all" onClick="checkAllCheckBox(this)"></td>
											<td><input type="checkbox" value="106" id="all" onClick="checkAllCheckBox(this)"></td>

										</tr>
										<tr>
											<td >Ca 1</td>
											<td><input name="checkboxarr[]" type="checkbox" value="0" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="1" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="2" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="3" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="4" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="5" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="6" id="test"></td>
										</tr>
										<tr>
											<td>Ca 2</td>
											<td><input name="checkboxarr[]" type="checkbox" value="7" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="8" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="9" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="10" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="11" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="12" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="13" id="test"></td>
										</tr>
										<tr>
											<td>Ca 3</td>
											<td><input name="checkboxarr[]" type="checkbox" value="14" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="15" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="16" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="17" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="18" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="19" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="20" id="test"></td>
										</tr>
										<tr>
											<td>Ca 4</td>
											<td><input name="checkboxarr[]" type="checkbox" value="21" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="22" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="23" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="24" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="25" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="26" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="27" id="test"></td>
										</tr>
										<tr>
											<td>Ca 5</td>
											<td><input name="checkboxarr[]" type="checkbox" value="28" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="29" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="30" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="31" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="32" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="33" id="test"></td>
											<td><input name="checkboxarr[]" type="checkbox" value="34" id="test"></td>
										</tr>
									</tbody>
								</table>
							</div><!--End of CSSTable Generator-->
							<div align="right">
								<br>
                            	<input type="submit" class="btn btn-warning" name="schedulebt" id="schedulebt" value="Gửi/Sửa Lịch học">
								
							</div>
						</form><!--End of Form submit schedule-->
                      
                        <?php 
						
						if(isset($_POST['schedulebt']))
						{
							
							if(!$_SESSION['user'])
								echo"<script>alert('Bạn phải Đăng nhập để có thể gửi Lịch học'); </script>";
							else
							{ 
							$array = $_POST['checkboxarr'];
								if(count($array) == 0)
									echo"<script>alert('Vui lòng chọn lịch học của bạn'); </script>";
								else
								{
									$addStd = $t->addStdList($topicID, $_SESSION['user']);
	
									if (!$flag) {$t->addScheStd($_SESSION['user'],$topicID);}
									
									echo "<script>  alert('Gửi lịch học thành công'); </script>";
									$t->alterScheduleStd($_SESSION['user'],$topicID,$array);
									//$t->addSchedule($array,$topicID);
									
									echo "<script>
									 window.location.href='thread.php?topicID=".$topicID."'; </script>";
								}
							
							
							
							/*
							   <input type="hidden" id="topicID" name="topicID" value="<?php echo $_GET['topicID']; ?>" />
								if($t->checkboxNull($_GET['checkboxarr'])) //how to get checkbox array form
								{
									echo"<script>alert('Vui lòng chọn lịch học của bạn'); </script>";
								}
								else
								{
									echo "OK";
									//$addStd = $t->addStdList($_GET['topicID'],$_SESSION['user']);
									echo"<script>alert('Thêm Lịch học thành công!'); </script>";
									
								}*/
							}
						}
						?>
					</div><!--End of Schedule col-->
				</div>
			</div>
		</div><!--End of panel-->
		<hr color="#0000FF">

	</div><!-- End of container-->

</body>
</html>
<!-- con thoi gian se lam linking toi may trang cua khoa, truong, sakai, group facebook//
lam them session vua moi vao gan day load khoang 10 sinh viên, ham co dang string nối chuỗi, rồi ở dưới chỉ cần echo tên hàm
thêm vào bằng cách login thì gọi hàm thêm vào db, db có dạng sửa num ưu tiên-->