
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>TDT Learning Group</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

	<link rel="stylesheet" type="text/css" href="css/mystyle.css" />
   <!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="//v2.zopim.com/?2u6ZeLO3mqxZk35wqk1IDsBMr2AxwKv0";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <!--End of Zopim Live Chat Script-->
</head>
<body style="background-color: #aaf0d1">
	<div class="container">
		<div class="row">
			<div class="col-md-3" style="text-align:center">
				<img src="img/logo.png" alt="TDT Learning Group" width = 200, height = 110 onClick="window.location.href = 'index.php'">
				<?php 		
				session_start();	
				ini_set("display_errors",0);
				if(!$_SESSION['user'])
				{
				?>				
					<br><br>
					<button class="btn btn-lg btn-success" name="loginbt" id="loginbt" onclick=window.location.href="login.php"  style="width:200px;">Log in</button>
					<?php
				} else {
					echo "<h3><span class='label label-info'  onClick=\"window.location.href='changepass.php'\">".$_SESSION['user']."</span></h3>"; // format css
					?> 
					
					
					<br>
					<button class="btn btn-lg btn-danger" name="logoutbt" id="logoutbt" onclick=window.location.href="logout.php"  style="width:200px;">Log out</button>
					
					<?php			
				} 
				
				?>
				
			</div>
			
			<div class="col-md-9">
				<div id="mycarousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#mycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#mycarousel" data-slide-to="1"></li>
						<li data-target="#mycarousel" data-slide-to="2"></li>
						<li data-target="#mycarousel" data-slide-to="3"></li>
						<li data-target="#mycarousel" data-slide-to="4"></li>
						<li data-target="#mycarousel" data-slide-to="5"></li>

					</ol>

					<div class="carousel-inner" role="listbox" align="center">
						<div class="item active"><img src="img/1.jpg" width="460" height="345"></div>
						<div class="item"><img src="img/2.jpg" width="460" height="345"></div>
						<div class="item"><img src="img/3.jpg" width="460" height="345"></div>
						<div class="item"><img src="img/4.jpg" width="460" height="345"></div>
						<div class="item"><img src="img/5.jpg" width="460" height="345"></div>
						<div class="item"><img src="img/6.jpg" width="460" height="345"></div>
					</div>
					<a class="left carousel-control" href="#mycarousel" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">previous</span>
					</a>
					<a class="right carousel-control" href="#mycarousel" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">next</span>
					</a>
					<script src="ajax/jquery.min.js"></script>
					<script src="js/bootstrap.js" ></script>
				</div>
			</div><!--Carousel-->
		</div>
		
	</div>

</body>
</html>